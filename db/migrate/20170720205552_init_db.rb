class InitDb < ActiveRecord::Migration[5.1]
  def up
    execute "CREATE TABLE `posts` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `title` varchar(255) DEFAULT NULL,
      `seo_title` varchar(255) DEFAULT NULL,
      `seo_desc` text,
      `seo_keyword` text,
      `content` text,
      `state` tinyint(4) DEFAULT '0',
      `pos` int(11) DEFAULT NULL,
      `user_id` int(11) DEFAULT NULL,
      `created_at` datetime DEFAULT NULL,
      `updated_at` datetime DEFAULT NULL,
      `short` text,
      `main_photo_id` int(11) DEFAULT NULL,
      `short_url` varchar(255) DEFAULT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;"

  execute "CREATE TABLE `photos` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `post_id` int(11) DEFAULT NULL,
      `image` varchar(255) DEFAULT NULL,
      `created_at` datetime DEFAULT NULL,
      `updated_at` datetime DEFAULT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;
    "
  end

  def down
     execute "DROP TABLE `posts`;"
     execute "DROP TABLE `photos`;"
  end
end
