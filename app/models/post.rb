class Post < ApplicationRecord
  has_many   :photos,  :dependent => :destroy
  belongs_to  :main_photo, :class_name => 'Photo',   required: false
  has_and_belongs_to_many :tags


  def main_image
    if  main_photo && main_photo.image
      main_photo.image_url
    else
      nil
    end
  end

  def prev(limit=1)
     item = Post.where(state: 1).where('id < ?', id ).last(limit)
     item = Post.where(state: 1).first(limit) if item == []
     limit == 1 ? item[0] : item
  end

  def next(limit=1)
    item = Post.where(state: 1).where('id > ?', id ).first(limit)
    item = Post.where(state: 1).first(limit) if item == []
    limit == 1 ? item[0] : item
  end

end
