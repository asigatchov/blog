class Photo < ApplicationRecord
  belongs_to :post
	has_one :main_to_post, :class_name => 'Post', :foreign_key => :main_photo_id, :dependent => :nullify
	mount_uploader :image, PhotoUploader

end
