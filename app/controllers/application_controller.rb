class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception
  before_action :left_post
  before_action :authenticate_user!, except: [:index, :show, :seo_show]
  

  def left_post
   @left_post = Post.where(state: 1).last(3)
   @left_post.where.not(id: @post.id) if @post
   @tags = Tag.active 
  end

  def is_admin?
    redirect_to root_path unless current_user.try(:admin?)
  end
end
