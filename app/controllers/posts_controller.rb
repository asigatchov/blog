class PostsController < ApplicationController
  before_action :find_post, except: [:index, :create, :new, :seo_show]
  before_action :init_page
  before_action :is_admin?, only: [:edit, :update, :destroy, :new]

  def index
    state = [1]
    state << 0 if current_user.try(:admin?)
    @seo_title = 'Блог  Сигачева Александра. Страница ' + @page.to_s
    @posts = []
    if params[:title]
      tag = Tag.find_by(title: params[:title])
      @posts = tag.posts.order('id desc').paginate(page: @page)
    else
      @posts = Post.where(state: state).order('id desc').paginate(page: @page)
    end
    ids = @posts.map(&:id)
    @seo_desc =  @posts.map(&:title).join(' ')
    @seo_keywords = @seo_desc.split(/\s+/).uniq.select{|w| w.size > 5 }.join(', ')
    @left_post = Post.where(state: 1).where.not(id: ids).last(3)
  end

  def show
    @seo_title = @post.seo_title
    @seo_desc = @post.seo_desc
    @seo_keywords = @post.seo_keyword
  end

  def seo_show
    left_post
    @post = Post.find_by(title: params[:title])
    @seo_title = @post.seo_title
    @seo_desc = @post.seo_desc
    @seo_keywords = @post.seo_keyword
    render 'posts/show'
  end

  def edit

  end

  def update
    if 	@post.update_attributes(post_permit)
      photos = add_photos(@post)

      if !@post.main_photo_id && photos
        @post.update_attributes(main_photo_id: photos.sample.id)
      end  

      @post.tags.destroy_all
      Tag.where(id: params[:tag_ids]).each do |tag|
         @post.tags << tag
      end
      @post.save
      redirect_to edit_post_path(@post)
    else
      render :edit
    end

  end

  def new
    @post = Post.new
  end

  def create
    if  @post =	Post.create(post_permit)
      photos = add_photos(@post)
      if !@post.main_photo_id && photos
        @post.update_attributes(main_photo_id: photos.sample.id)
      end  

      Tag.where(id: params[:tag_ids]).each do |tag|
        @post.tags << tag
      end
    end
  end

  private

  def add_photos(post)
     return  false  if params[:post][:photos].blank?
     params[:post][:photos].map do |i|
       photo =  Photo.new(post_id: post.id)
       photo.image = i['image']
       photo.save
       photo
     end
  end


  def init_page
    @page = [1, params[:page].to_i ].max
  end

  def find_post
    @post = Post.find(params[:id])
  end

  def post_permit
    params[:post].permit(:title, :seo_title, :seo_desc, :seo_keyword, :short, :state, :content, :pos )
  end
end
