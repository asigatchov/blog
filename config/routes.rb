Rails.application.routes.draw do
  devise_for :users

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root :to => 'posts#index'
  resources :posts
  resources :photos

  get '/рубрика/:title', to: 'posts#index', as: 'catigories'
  get '/статья/:title', to: 'posts#seo_show', as: 'seo_post', :constraints => {:title => /.+/}

end
