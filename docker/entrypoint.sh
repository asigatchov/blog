#!/bin/bash
set -eo pipefail
shopt -s nullglob

chown -R app. /app/ -R

export HOME=/app
exec setuid  app "$@"
